package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.SelfSupportOrderInfoService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月5日
 */
@RestController
public class SelfSupportOrderInfoController {
    
    @Autowired
    private SelfSupportOrderInfoService orderInfoService;
    
    /**
     * 条件查询自营订单商品列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-order-info/selection", method = RequestMethod.GET)
    public Result getAllSupportOrderInfo(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
	return orderInfoService.listPage(pageNum, pageSize, params);
    }

    /**
     * id查询自营订单商品的详情信息
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-order-info/detail", method = RequestMethod.GET)
    public Result getOrderInfoByPrimaryKey(@RequestParam String id) {
	return orderInfoService.selectByPrimaryKey(id);
    }
    
}
