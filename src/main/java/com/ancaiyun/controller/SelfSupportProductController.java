package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.SelfSupportProductService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月2日
 */
@RestController
public class SelfSupportProductController {
    
    @Autowired
    private SelfSupportProductService productService;
    
    /**
     * 条件查询自营商品列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-product/selection",method = RequestMethod.GET)
    public Result getAllProduct(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return productService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 查询自营商品详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-product/detail",method = RequestMethod.GET)
    public Result getProductByPrimaryKey(@RequestParam String id) {
        return productService.selectByPrimaryKey(id);
    }
    
    /**
     * 添加自营商品
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-product/insert",method = RequestMethod.POST)
    public Result insertProduct(@RequestParam Map<String, Object> params) {
        return productService.insertProduct(params);
    }
    
    /**
     * 修改自营商品
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-product/update",method = RequestMethod.PUT)
    public Result updateProduct(@RequestParam Map<String, Object> params) {
        return productService.updateProduct(params);
    }
}
