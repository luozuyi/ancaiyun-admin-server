package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.SelfSupportStoreService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月2日
 */
@RestController
public class SelfSupportStoreController {
    
    @Autowired
    private SelfSupportStoreService storeService;
    
    
    /**
     * 查询自营店铺详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-store/detail",method = RequestMethod.GET)
    public Result getStoreByPrimaryKey(@RequestParam String id) {
        return storeService.selectByPrimaryKey(id);
    }
    
    /**
     * 修改店铺
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-store/update",method = RequestMethod.PUT)
    public Result updateStore(@RequestParam Map<String, Object> params) {
        return storeService.updateStore(params);
    }
}
