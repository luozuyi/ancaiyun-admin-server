package com.ancaiyun.controller;

import com.ancaiyun.service.AncailogisticsService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AncailogisticsManagerController {

    @Autowired
    private AncailogisticsService ancailogisticsService;

    /**
     * 分页查询物流列表
     * @param pageNum 当前页数
     * @param pageSize 一页显示多少条
     * @param params 查询参数
     * @return
     */
    @GetMapping("v1/auth/ancailogistics")
    public Result listPage(Integer pageNum, Integer pageSize, @RequestParam Map<String, String> params){
        return ancailogisticsService.listPage(pageNum, pageSize, params);
    }

    /**
     * 分页查询安采物流发货列表
     * @param pageNum 当前页数
     * @param pageSize 一页显示多少条
     * @param params 查询参数
     * @return
     */
    @GetMapping("v1/auth/ancailogistics/goods")
    public Result sendGoodsListPage(Integer pageNum, Integer pageSize, @RequestParam Map<String, String> params){
        return ancailogisticsService.sendGoodsListPage(pageNum, pageSize, params);
    }

    /**
     * 修改物流状态
     * @param id 主键id
     * @param transactionStatus 交易状态 0:待联系 1:达成交易 2:未达成交易
     * @param logisticsStatus 物流状态 0:待发货 1:已发货 2:已收货
     * @return
     */
    @PutMapping("v1/auth/ancailogistics")
    public Result updateStatus(String id, String transactionStatus, String logisticsStatus){
        return ancailogisticsService.updateStatus(id, transactionStatus, logisticsStatus);
    }
}
