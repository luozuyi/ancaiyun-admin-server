package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.QuickPurchaseService;
import com.ancaiyun.utils.Result;

/**
 * 极速采购
 * @author Catch22
 * @date 2018年6月8日
 */
@RestController
public class QuickPurchaseController {
    
    @Autowired
    private QuickPurchaseService quickPurchaseService;
    
    /**
     * 条件查询所有采购
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/qucik-purchase/selection",method = RequestMethod.GET)
    public Result getAllProduct(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return quickPurchaseService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 查询采购详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/qucik-purchase/detail",method = RequestMethod.GET)
    public Result getProductByPrimaryKey(@RequestParam String id) {
        return quickPurchaseService.selectByPrimaryKey(id);
    }
    
}
