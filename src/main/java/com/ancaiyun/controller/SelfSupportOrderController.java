package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.SelfSupportOrderService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月5日
 */
@RestController
public class SelfSupportOrderController {

    @Autowired
    private SelfSupportOrderService orderService;

    /**
     * 条件查询自营订单列表
     * 
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-order/selection", method = RequestMethod.GET)
    public Result getAllSupportOrder(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
	return orderService.listPage(pageNum, pageSize, params);
    }

    /**
     * id查询自营订单详情信息
     * 
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-order/detail", method = RequestMethod.GET)
    public Result getOrderByPrimaryKey(@RequestParam String id) {
	return orderService.selectByPrimaryKey(id);
    }

    /**
     * 确认订单
     * 
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/self-support-order/confirm", method = RequestMethod.POST)
    public Result confirmSupportOrder(@RequestParam Map<String, Object> params) {
	return orderService.update(params);
    }
}
