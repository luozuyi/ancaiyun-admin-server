package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.StoreTemplateService;
import com.ancaiyun.utils.Result;

/**
 * 店铺模板管理controller
 * @author Catch22
 * @date 2018年6月8日
 */
@RestController
public class StoreTemplateController {
    
    @Autowired
    private StoreTemplateService templateService;
    
    /**
     *  查询店铺模板列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/store-template/selection",method = RequestMethod.GET)
    public Result getAllTemplate(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return templateService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 查询店铺模板详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/store-template/detail",method = RequestMethod.GET)
    public Result getProductByPrimaryKey(@RequestParam String id) {
        return templateService.selectByPrimaryKey(id);
    }
    
    /**
     * 添加店铺模板
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/store-template/insert",method = RequestMethod.POST)
    public Result insertProduct(@RequestParam Map<String, Object> params) {
        return templateService.insertTemplate(params);
    }
    
    /**
     * 删除店铺模板
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/store-template/del",method = RequestMethod.PUT)
    public Result updateProduct(@RequestParam Map<String, Object> params) {
        return templateService.delteTemplate(params);
    }
}
