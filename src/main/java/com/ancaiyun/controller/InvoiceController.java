package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.InvoiceService;
import com.ancaiyun.utils.Result;

/**
 * 发票管理
 * @author Catch22
 * @date 2018年6月8日
 */
@RestController
public class InvoiceController {
    
    @Autowired
    private InvoiceService invoiceService;
    
    /**
     * 条件按查询已开发票列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/invoice/selection",method = RequestMethod.GET)
    public Result getAllProduct(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return invoiceService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * id查询发票详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/invoice/detail",method = RequestMethod.GET)
    public Result getProductByPrimaryKey(@RequestParam String id) {
        return invoiceService.selectByPrimaryKey(id);
    }
    
    /**
     * 自营商城开发票
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/invoice/insert",method = RequestMethod.POST)
    public Result insertInvoice(@RequestParam Map<String, Object> params) {
        return invoiceService.insertInvoice(params);
    }

}
