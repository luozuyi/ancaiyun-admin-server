package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.ProductCollectService;
import com.ancaiyun.service.StoreCollectService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月4日
 */
@RestController
public class CollectController {
    
    @Autowired
    private StoreCollectService storeCollectService;
    
    @Autowired
    private ProductCollectService productCollectService;
    
    /**
     * 店铺收藏列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/collect/store",method = RequestMethod.GET)
    public Result getStoreCollect(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return storeCollectService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 商品收藏列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/collect/product",method = RequestMethod.GET)
    public Result getProductCollect(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return productCollectService.listPage(pageNum, pageSize, params);
    }
}
