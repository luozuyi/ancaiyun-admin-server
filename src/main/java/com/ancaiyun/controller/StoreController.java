package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.StoreService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月2日
 */
@RestController
public class StoreController {
    
    @Autowired
    private StoreService storeService;
    
    /**
     * 条件查询店铺列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/store/selection",method = RequestMethod.GET)
    public Result getAllStore(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return storeService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 查询店铺详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/store/detail",method = RequestMethod.GET)
    public Result getStoreByPrimaryKey(@RequestParam String id) {
        return storeService.selectByPrimaryKey(id);
    }
    
    /**
     * 修改店铺
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/store/update",method = RequestMethod.PUT)
    public Result updateStore(@RequestParam Map<String, Object> params) {
        return storeService.updateStore(params);
    }
}
