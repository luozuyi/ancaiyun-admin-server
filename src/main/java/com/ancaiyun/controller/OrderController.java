package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.OrderService;
import com.ancaiyun.utils.Result;

/**
 * @author Catch22
 * @date 2018年6月6日
 */
@RestController
public class OrderController {
    
    @Autowired
    private OrderService orederService;
    
    /**
     * 条件查询订单列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/order/selection",method = RequestMethod.GET)
    public Result getAllProduct(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return orederService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/order/detail",method = RequestMethod.GET)
    public Result getProductByPrimaryKey(@RequestParam String id) {
        return orederService.selectByPrimaryKey(id);
    }
}
