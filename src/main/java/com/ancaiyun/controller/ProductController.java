package com.ancaiyun.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ancaiyun.service.ProductService;
import com.ancaiyun.utils.Result;

/**
 * 商品管理
 * @author Catch22
 * @date 2018年6月2日
 */
@RestController
public class ProductController {
    
    @Autowired
    private ProductService productService;
    
    /**
     * 条件查询商品列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/product/selection",method = RequestMethod.GET)
    public Result getAllProduct(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return productService.listPage(pageNum, pageSize, params);
    }
    
    /**
     * 查询商品详情
     * @param id
     * @return
     */
    @RequestMapping(value = "v1/auth/product/detail",method = RequestMethod.GET)
    public Result getProductByPrimaryKey(@RequestParam String id) {
        return productService.selectProductByPrimaryKey(id);
    }
    
    /**
     * 修改商品上下架/违规处理
     * @param params
     * @return
     */
    @RequestMapping(value = "v1/auth/product/update",method = RequestMethod.POST)
    public Result updateProduct(@RequestParam Map<String, Object> params) {
        return productService.updateProduct(params);
    }
}
