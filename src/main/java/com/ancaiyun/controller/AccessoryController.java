package com.ancaiyun.controller;

import com.ancaiyun.service.AccessoryService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AccessoryController {
    @Autowired
    private AccessoryService accessoryService;

    /**
     * 上传文章封面
     * @param file 文件
     * @return
     */
    @RequestMapping(value = "v1/auth/accessorys",method = RequestMethod.POST)
    public Result addArticleCover(MultipartFile file){
        return accessoryService.addArticleCover(file);
    }
}
