package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;

public class QuickPurchase implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 填写的数据
     */
    private String something;
    /**
     * 电话
     */
    private String telphone;
    /**
     * 子用户名
     */
    private String subUsername;
    /**
     * 采购物资清单id
     */
    private String coverId;
    /**
     * 采购状态  0 ：待联系   1 ：已达成    -1 ：未达成 
     */
    private String status;
    /**
     * 物流状态  0 ：待发货  1 ：已发货   2 ： 已收货
     */
    private String logisticsStatus;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getSomething() {
	return something;
    }

    public void setSomething(String something) {
	this.something = something;
    }

    public String getTelphone() {
	return telphone;
    }

    public void setTelphone(String telphone) {
	this.telphone = telphone;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

    public String getCoverId() {
	return coverId;
    }

    public void setCoverId(String coverId) {
	this.coverId = coverId;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getLogisticsStatus() {
	return logisticsStatus;
    }

    public void setLogisticsStatus(String logisticsStatus) {
	this.logisticsStatus = logisticsStatus;
    }

}
