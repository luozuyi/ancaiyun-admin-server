package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单商品
 * 
 * @author Catch22
 * @date 2018年6月6日
 */
public class OrderProductInfo implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 商品数量
     */
    private Integer productCount;
    /**
     * 商品价格
     */
    private BigDecimal productPrice;
    /**
     * 商品外键 一对一
     */
    private String productId;
    /**
     * 订单 多对一
     */
    private String orderId;
    /**
     * 子用户名
     */
    private String subUsername;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getOrderCode() {
	return orderCode;
    }

    public void setOrderCode(String orderCode) {
	this.orderCode = orderCode;
    }

    public Integer getProductCount() {
	return productCount;
    }

    public void setProductCount(Integer productCount) {
	this.productCount = productCount;
    }

    public BigDecimal getProductPrice() {
	return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
	this.productPrice = productPrice;
    }

    public String getProductId() {
	return productId;
    }

    public void setProductId(String productId) {
	this.productId = productId;
    }

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

}
