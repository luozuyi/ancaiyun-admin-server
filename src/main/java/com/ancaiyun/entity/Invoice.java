package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 发票实体类
 * 
 * @author Catch22
 * @date 2018年6月8日
 */
public class Invoice implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 单位名称
     */
    private String companyName;
    /**
     * 纳税人识别号
     */
    private String taxpayerCode;
    /**
     * 注册地址
     */
    private String registAdress;
    /**
     * 注册电话
     */
    private String registTelphone;
    /**
     * 开户银行
     */
    private String bankName;
    /**
     * 开户银行账户
     */
    private String bankCode;
    /**
     * 邮寄省
     */
    private String destinationProvince;
    /**
     * 邮寄区
     */
    private String destinationCountry;
    /**
     * 邮寄市
     */
    private String destinationCity;
    /**
     * 详细收票地址
     */
    private String detailDestinationAdress;
    /**
     * 收票人
     */
    private String recivePerson;
    /**
     * 收票人联系方式
     */
    private String personTelphone;
    /**
     * 相关id
     */
    private String relationId;
    /**
     * 发票类型
     */
    private String type;
    /**
     * 发票场景
     */
    private String scene;
    /**
     * 发票状态
     */
    private String status;
    /**
     * 会员Id
     */
    private String memberId;
    /**
     * 实际开票额
     */
    private BigDecimal price;
    /**
     * 发票号
     */
    private String invoiceCode;
    /**
     * 索取时间
     */
    private Date askTime;
    /**
     * 子用户名
     */
    private String subUsername;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getCompanyName() {
	return companyName;
    }

    public void setCompanyName(String companyName) {
	this.companyName = companyName;
    }

    public String getTaxpayerCode() {
	return taxpayerCode;
    }

    public void setTaxpayerCode(String taxpayerCode) {
	this.taxpayerCode = taxpayerCode;
    }

    public String getRegistAdress() {
	return registAdress;
    }

    public void setRegistAdress(String registAdress) {
	this.registAdress = registAdress;
    }

    public String getRegistTelphone() {
	return registTelphone;
    }

    public void setRegistTelphone(String registTelphone) {
	this.registTelphone = registTelphone;
    }

    public String getBankName() {
	return bankName;
    }

    public void setBankName(String bankName) {
	this.bankName = bankName;
    }

    public String getBankCode() {
	return bankCode;
    }

    public void setBankCode(String bankCode) {
	this.bankCode = bankCode;
    }

    public String getDestinationProvince() {
	return destinationProvince;
    }

    public void setDestinationProvince(String destinationProvince) {
	this.destinationProvince = destinationProvince;
    }

    public String getDestinationCountry() {
	return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
	this.destinationCountry = destinationCountry;
    }

    public String getDestinationCity() {
	return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
	this.destinationCity = destinationCity;
    }

    public String getDetailDestinationAdress() {
	return detailDestinationAdress;
    }

    public void setDetailDestinationAdress(String detailDestinationAdress) {
	this.detailDestinationAdress = detailDestinationAdress;
    }

    public String getRecivePerson() {
	return recivePerson;
    }

    public void setRecivePerson(String recivePerson) {
	this.recivePerson = recivePerson;
    }

    public String getPersonTelphone() {
	return personTelphone;
    }

    public void setPersonTelphone(String personTelphone) {
	this.personTelphone = personTelphone;
    }

    public String getRelationId() {
	return relationId;
    }

    public void setRelationId(String relationId) {
	this.relationId = relationId;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getScene() {
	return scene;
    }

    public void setScene(String scene) {
	this.scene = scene;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getMemberId() {
	return memberId;
    }

    public void setMemberId(String memberId) {
	this.memberId = memberId;
    }

    public BigDecimal getPrice() {
	return price;
    }

    public void setPrice(BigDecimal price) {
	this.price = price;
    }

    public String getInvoiceCode() {
	return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
	this.invoiceCode = invoiceCode;
    }

    public Date getAskTime() {
	return askTime;
    }

    public void setAskTime(Date askTime) {
	this.askTime = askTime;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

}
