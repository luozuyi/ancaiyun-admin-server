package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * 商品
 */
public class Product implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 浏览量
     */
    private Integer browserNumber;
    /**
     * 原价格
     */
    private BigDecimal productPrice;
    /**
     * 折扣价
     */
    private BigDecimal bargainPrice;
    /**
     * 关键字
     */
    private String keywords;
    /**
     * 商品库存量
     */
    private Double productInventory;
    /**
     * 商品库存详情
     */
    private String productInventoryDetail;
    /**
     * 销量
     */
    private Double saleNum;
    /**
     * 商品编码
     */
    private String productSerial;
    /**
     * 重量
     */
    private BigDecimal productWeight;
    /**
     * 体积
     */
    private BigDecimal productVolume;
    /**
     * 商品详细描述
     */
    private String productDetail;
    /**
     * 收藏量
     */
    private Integer productCollects;
    /**
     * 商品状态1:未上架的商品2：销售中的商品3：违规的商品
     */
    private Integer productStatus;
    /**
     * 类型
     */
    private String productSubCategoryId;
    /**
     * 品牌
     */
    private String productBrandId;
    /**
     * 店铺
     */
    private String storeId;
    /**
     * 推广
     */
    private String promoteTitle;
    /**
     * 计量数值
     */
    private String countNum;
    /**
     * 仓库
     */
    private String warehouseId;
    /**
     * 规格
     */
    private String specificationId;
    /**
     * 材质
     */
    private String materialsId;
    /**
     * 品牌手动填写
     */
    private String brand;
    /**
     * 属性手动填写
     */
    private String property;
    /**
     * 属性值手动填写
     */
    private String propertyValue;
    /**
     * 单位手动填写
     */
    private String productUnit;
    /**
     * 子用户名
     */
    private String subUsername;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getProductName() {
	return productName;
    }

    public void setProductName(String productName) {
	this.productName = productName;
    }

    public Integer getBrowserNumber() {
	return browserNumber;
    }

    public void setBrowserNumber(Integer browserNumber) {
	this.browserNumber = browserNumber;
    }

    public BigDecimal getProductPrice() {
	return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
	this.productPrice = productPrice;
    }

    public BigDecimal getBargainPrice() {
	return bargainPrice;
    }

    public void setBargainPrice(BigDecimal bargainPrice) {
	this.bargainPrice = bargainPrice;
    }

    public String getKeywords() {
	return keywords;
    }

    public void setKeywords(String keywords) {
	this.keywords = keywords;
    }

    public Double getProductInventory() {
	return productInventory;
    }

    public void setProductInventory(Double productInventory) {
	this.productInventory = productInventory;
    }

    public String getProductInventoryDetail() {
	return productInventoryDetail;
    }

    public void setProductInventoryDetail(String productInventoryDetail) {
	this.productInventoryDetail = productInventoryDetail;
    }

    public Double getSaleNum() {
	return saleNum;
    }

    public void setSaleNum(Double saleNum) {
	this.saleNum = saleNum;
    }

    public String getProductSerial() {
	return productSerial;
    }

    public void setProductSerial(String productSerial) {
	this.productSerial = productSerial;
    }

    public BigDecimal getProductWeight() {
	return productWeight;
    }

    public void setProductWeight(BigDecimal productWeight) {
	this.productWeight = productWeight;
    }

    public BigDecimal getProductVolume() {
	return productVolume;
    }

    public void setProductVolume(BigDecimal productVolume) {
	this.productVolume = productVolume;
    }

    public String getProductDetail() {
	return productDetail;
    }

    public void setProductDetail(String productDetail) {
	this.productDetail = productDetail;
    }

    public Integer getProductCollects() {
	return productCollects;
    }

    public void setProductCollects(Integer productCollects) {
	this.productCollects = productCollects;
    }

    public Integer getProductStatus() {
	return productStatus;
    }

    public void setProductStatus(Integer productStatus) {
	this.productStatus = productStatus;
    }

    public String getProductSubCategoryId() {
	return productSubCategoryId;
    }

    public void setProductSubCategoryId(String productSubCategoryId) {
	this.productSubCategoryId = productSubCategoryId;
    }

    public String getProductBrandId() {
	return productBrandId;
    }

    public void setProductBrandId(String productBrandId) {
	this.productBrandId = productBrandId;
    }

    public String getStoreId() {
	return storeId;
    }

    public void setStoreId(String storeId) {
	this.storeId = storeId;
    }

    public String getPromoteTitle() {
	return promoteTitle;
    }

    public void setPromoteTitle(String promoteTitle) {
	this.promoteTitle = promoteTitle;
    }

    public String getCountNum() {
	return countNum;
    }

    public void setCountNum(String countNum) {
	this.countNum = countNum;
    }

    public String getWarehouseId() {
	return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
	this.warehouseId = warehouseId;
    }

    public String getSpecificationId() {
	return specificationId;
    }

    public void setSpecificationId(String specificationId) {
	this.specificationId = specificationId;
    }

    public String getMaterialsId() {
	return materialsId;
    }

    public void setMaterialsId(String materialsId) {
	this.materialsId = materialsId;
    }

    public String getBrand() {
	return brand;
    }

    public void setBrand(String brand) {
	this.brand = brand;
    }

    public String getProperty() {
	return property;
    }

    public void setProperty(String property) {
	this.property = property;
    }

    public String getPropertyValue() {
	return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
	this.propertyValue = propertyValue;
    }

    public String getProductUnit() {
	return productUnit;
    }

    public void setProductUnit(String productUnit) {
	this.productUnit = productUnit;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

    public void preInsert() {
	this.setId(UUID.randomUUID().toString().replaceAll("-", ""));
	this.setCreateTime(new Date());
	this.setDelFlag("0");
    }
}
