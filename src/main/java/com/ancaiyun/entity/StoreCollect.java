package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * 
 * 收藏店铺
 */
public class StoreCollect implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 会员
     */
    private String memberId;
    /**
     * 商铺
     */
    private String storeId;
    /**
     * 子用户名
     */
    private String subUsername;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getMemberId() {
	return memberId;
    }

    public void setMemberId(String memberId) {
	this.memberId = memberId;
    }

    public String getStoreId() {
	return storeId;
    }

    public void setStoreId(String storeId) {
	this.storeId = storeId;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

}
