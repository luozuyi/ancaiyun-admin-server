package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 自营商城订单详情
 * @author Catch22
 * @date 2018年6月4日
 */
public class SelfSupportOrderInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 商品数量
     */
    private Integer productCount;
    /**
     * 自营商品
     */
    private String selfSupportProductId;
    /**
     * 自营订单
     */
    private String selfSupportOrderId;
    /**
     * 小计
     */
    private BigDecimal selfSubtotalprice;
    /**
     * 单价
     */
    private BigDecimal selfUnitPrice;
    /**
     * 子用户名
     */
    private String subUsername;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getOrderCode() {
	return orderCode;
    }

    public void setOrderCode(String orderCode) {
	this.orderCode = orderCode;
    }

    public Integer getProductCount() {
	return productCount;
    }

    public void setProductCount(Integer productCount) {
	this.productCount = productCount;
    }

    public String getSelfSupportProductId() {
	return selfSupportProductId;
    }

    public void setSelfSupportProductId(String selfSupportProductId) {
	this.selfSupportProductId = selfSupportProductId;
    }

    public String getSelfSupportOrderId() {
	return selfSupportOrderId;
    }

    public void setSelfSupportOrderId(String selfSupportOrderId) {
	this.selfSupportOrderId = selfSupportOrderId;
    }

    public BigDecimal getSelfSubtotalprice() {
	return selfSubtotalprice;
    }

    public void setSelfSubtotalprice(BigDecimal selfSubtotalprice) {
	this.selfSubtotalprice = selfSubtotalprice;
    }

    public BigDecimal getSelfUnitPrice() {
	return selfUnitPrice;
    }

    public void setSelfUnitPrice(BigDecimal selfUnitPrice) {
	this.selfUnitPrice = selfUnitPrice;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

}
