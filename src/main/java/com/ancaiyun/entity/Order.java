package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Order implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 订单编号
     */
    private String orderCode;
    /**
     * 手续费
     */
    private BigDecimal productFee;
    /**
     * 订单总价
     */
    private BigDecimal totalPrice;
    /**
     * 订单运送方式
     */
    private String orderDeliveryWay;
    /**
     * 订单结束时间
     */
    private Date endTime;
    /**
     * 异常状态 
     * 买家：待确认 买家：已确认 买家：已发货 买家：已收货 买家：已付款 买家：已完成 
     * 卖家：已下单 卖家：待发货 卖家：待收货 卖家：待付款 卖家：已完成
     */
    private String abnormalStatus;
    /**
     * 订单状态 0：待确认 1：待付款 2：待发货 3：待收货 4：已完成 5：已取消
     */
    private String status;
    /**
     * 会员
     */
    private String memberId;
    /**
     * 订单跟踪员
     */
    private String belongSale;
    /**
     * 订单付款方式,0：转账，1：现金，2：其他
     */
    private String orderPaymentType;
    /**
     * 收货地址
     */
    private String shippingAddress;
    /**
     * 交易方式
     */
    private String exchangeStatus;
    /**
     * 卖家ID
     */
    private String saleMemberId;
    /**
     * 子用户名
     */
    private String subUsername;
    
    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getOrderCode() {
	return orderCode;
    }

    public void setOrderCode(String orderCode) {
	this.orderCode = orderCode;
    }

    public BigDecimal getProductFee() {
	return productFee;
    }

    public void setProductFee(BigDecimal productFee) {
	this.productFee = productFee;
    }

    public BigDecimal getTotalPrice() {
	return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
	this.totalPrice = totalPrice;
    }

    public String getOrderDeliveryWay() {
	return orderDeliveryWay;
    }

    public void setOrderDeliveryWay(String orderDeliveryWay) {
	this.orderDeliveryWay = orderDeliveryWay;
    }

    public Date getEndTime() {
	return endTime;
    }

    public void setEndTime(Date endTime) {
	this.endTime = endTime;
    }

    public String getAbnormalStatus() {
	return abnormalStatus;
    }

    public void setAbnormalStatus(String abnormalStatus) {
	this.abnormalStatus = abnormalStatus;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getMemberId() {
	return memberId;
    }

    public void setMemberId(String memberId) {
	this.memberId = memberId;
    }

    public String getBelongSale() {
	return belongSale;
    }

    public void setBelongSale(String belongSale) {
	this.belongSale = belongSale;
    }

    public String getOrderPaymentType() {
	return orderPaymentType;
    }

    public void setOrderPaymentType(String orderPaymentType) {
	this.orderPaymentType = orderPaymentType;
    }

    public String getShippingAddress() {
	return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
	this.shippingAddress = shippingAddress;
    }

    public String getExchangeStatus() {
	return exchangeStatus;
    }

    public void setExchangeStatus(String exchangeStatus) {
	this.exchangeStatus = exchangeStatus;
    }

    public String getSaleMemberId() {
	return saleMemberId;
    }

    public void setSaleMemberId(String saleMemberId) {
	this.saleMemberId = saleMemberId;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

}
