package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 自营商城商品
 * 
 * @author Catch22
 * @date 2018年6月4日
 */
public class SelfSupportProduct implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 浏览量
     */
    private Integer browserNumber;
    /**
     * 原价格
     */
    private BigDecimal productPrice;
    /**
     * 折扣价
     */
    private BigDecimal bargainPrice;
    /**
     * 商品库存量
     */
    private Double productInventory;
    /**
     * 关键字
     */
    private String keywords;
    /**
     * 商品编码
     */
    private String productSerial;
    /**
     * 商品详细描述
     */
    private String productDetail;
    /**
     * 收藏量
     */
    private Integer productCollects;
    /**
     * 商品状态
     */
    private String productStatus;
    /**
     * 类型
     */
    private String productSubCategoryId;
    /**
     * 品牌
     */
    private String productBrandId;
    /**
     * 自营店铺
     */
    private String selfSupportStoreId;
    /**
     * 推广
     */
    private String promoteTitle;
    /**
     * 计量数值
     */
    private String countNum;
    /**
     * 仓库
     */
    private String warehouseId;
    /**
     * 规格
     */
    private String specificationId;
    /**
     * 材质
     */
    private String materialsId;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getProductName() {
	return productName;
    }

    public void setProductName(String productName) {
	this.productName = productName;
    }

    public Integer getBrowserNumber() {
	return browserNumber;
    }

    public void setBrowserNumber(Integer browserNumber) {
	this.browserNumber = browserNumber;
    }

    public BigDecimal getProductPrice() {
	return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
	this.productPrice = productPrice;
    }

    public BigDecimal getBargainPrice() {
	return bargainPrice;
    }

    public void setBargainPrice(BigDecimal bargainPrice) {
	this.bargainPrice = bargainPrice;
    }

    public Double getProductInventory() {
	return productInventory;
    }

    public void setProductInventory(Double productInventory) {
	this.productInventory = productInventory;
    }

    public String getKeywords() {
	return keywords;
    }

    public void setKeywords(String keywords) {
	this.keywords = keywords;
    }

    public String getProductSerial() {
	return productSerial;
    }

    public void setProductSerial(String productSerial) {
	this.productSerial = productSerial;
    }

    public String getProductDetail() {
	return productDetail;
    }

    public void setProductDetail(String productDetail) {
	this.productDetail = productDetail;
    }

    public Integer getProductCollects() {
	return productCollects;
    }

    public void setProductCollects(Integer productCollects) {
	this.productCollects = productCollects;
    }

    public String getProductStatus() {
	return productStatus;
    }

    public void setProductStatus(String productStatus) {
	this.productStatus = productStatus;
    }

    public String getProductSubCategoryId() {
	return productSubCategoryId;
    }

    public void setProductSubCategoryId(String productSubCategoryId) {
	this.productSubCategoryId = productSubCategoryId;
    }

    public String getProductBrandId() {
	return productBrandId;
    }

    public void setProductBrandId(String productBrandId) {
	this.productBrandId = productBrandId;
    }

    public String getSelfSupportStoreId() {
	return selfSupportStoreId;
    }

    public void setSelfSupportStoreId(String selfSupportStoreId) {
	this.selfSupportStoreId = selfSupportStoreId;
    }

    public String getPromoteTitle() {
	return promoteTitle;
    }

    public void setPromoteTitle(String promoteTitle) {
	this.promoteTitle = promoteTitle;
    }

    public String getCountNum() {
	return countNum;
    }

    public void setCountNum(String countNum) {
	this.countNum = countNum;
    }

    public String getWarehouseId() {
	return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
	this.warehouseId = warehouseId;
    }

    public String getSpecificationId() {
	return specificationId;
    }

    public void setSpecificationId(String specificationId) {
	this.specificationId = specificationId;
    }

    public String getMaterialsId() {
	return materialsId;
    }

    public void setMaterialsId(String materialsId) {
	this.materialsId = materialsId;
    }

}
