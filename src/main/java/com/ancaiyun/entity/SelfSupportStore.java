package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 自营商城店铺
 * 
 * @author Catch22
 * @date 2018年6月4日
 */
public class SelfSupportStore implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 店铺名称
     */
    private String storeName;
    /**
     * 店主
     */
    private String storeOwer;
    /**
     * 店主名片
     */
    private String storeOwerCard;
    /**
     * 店铺电话
     */
    private String storeTelephone;
    /**
     * 店铺qq
     */
    private String storeQq;
    /**
     * 店铺网址
     */
    private String storeWw;
    /**
     * 店铺seo关键字
     */
    private String storeSeoKeywords;
    /**
     * 店铺描述
     */
    private String storeSeoDescription;
    /**
     * 店铺信息
     */
    private String storeInfo;
    /**
     * 店铺用户名
     */
    private String userName;
    /**
     * 店铺地址
     */
    private String storeAreaId;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 主营
     */
    private String mainBusiness;
    /**
     * 公司地区
     */
    private String companyAreaId;
    /**
     * 公司详细地址
     */
    private String companyAreaDetail;
    /**
     * 企业类型
     */
    private String companyType;
    /**
     * 法人代表
     */
    private String legalRepresentative;
    /**
     * 注册资金
     */
    private BigDecimal registMoney;
    /**
     * 成立日期
     */
    private Date establishmentTime;
    /**
     * 注册号
     */
    private String registCode;
    /**
     * 营业执照
     */
    private String businessLicenseId;
    /**
     * 公司简介
     */
    private String companyInfo;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getStoreName() {
	return storeName;
    }

    public void setStoreName(String storeName) {
	this.storeName = storeName;
    }

    public String getStoreOwer() {
	return storeOwer;
    }

    public void setStoreOwer(String storeOwer) {
	this.storeOwer = storeOwer;
    }

    public String getStoreOwerCard() {
	return storeOwerCard;
    }

    public void setStoreOwerCard(String storeOwerCard) {
	this.storeOwerCard = storeOwerCard;
    }

    public String getStoreTelephone() {
	return storeTelephone;
    }

    public void setStoreTelephone(String storeTelephone) {
	this.storeTelephone = storeTelephone;
    }

    public String getStoreQq() {
	return storeQq;
    }

    public void setStoreQq(String storeQq) {
	this.storeQq = storeQq;
    }

    public String getStoreWw() {
	return storeWw;
    }

    public void setStoreWw(String storeWw) {
	this.storeWw = storeWw;
    }

    public String getStoreSeoKeywords() {
	return storeSeoKeywords;
    }

    public void setStoreSeoKeywords(String storeSeoKeywords) {
	this.storeSeoKeywords = storeSeoKeywords;
    }

    public String getStoreSeoDescription() {
	return storeSeoDescription;
    }

    public void setStoreSeoDescription(String storeSeoDescription) {
	this.storeSeoDescription = storeSeoDescription;
    }

    public String getStoreInfo() {
	return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
	this.storeInfo = storeInfo;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public String getStoreAreaId() {
	return storeAreaId;
    }

    public void setStoreAreaId(String storeAreaId) {
	this.storeAreaId = storeAreaId;
    }

    public String getCompanyName() {
	return companyName;
    }

    public void setCompanyName(String companyName) {
	this.companyName = companyName;
    }

    public String getMainBusiness() {
	return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
	this.mainBusiness = mainBusiness;
    }

    public String getCompanyAreaId() {
	return companyAreaId;
    }

    public void setCompanyAreaId(String companyAreaId) {
	this.companyAreaId = companyAreaId;
    }

    public String getCompanyAreaDetail() {
	return companyAreaDetail;
    }

    public void setCompanyAreaDetail(String companyAreaDetail) {
	this.companyAreaDetail = companyAreaDetail;
    }

    public String getCompanyType() {
	return companyType;
    }

    public void setCompanyType(String companyType) {
	this.companyType = companyType;
    }

    public String getLegalRepresentative() {
	return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
	this.legalRepresentative = legalRepresentative;
    }

    public BigDecimal getRegistMoney() {
	return registMoney;
    }

    public void setRegistMoney(BigDecimal registMoney) {
	this.registMoney = registMoney;
    }

    public Date getEstablishmentTime() {
	return establishmentTime;
    }

    public void setEstablishmentTime(Date establishmentTime) {
	this.establishmentTime = establishmentTime;
    }

    public String getRegistCode() {
	return registCode;
    }

    public void setRegistCode(String registCode) {
	this.registCode = registCode;
    }

    public String getBusinessLicenseId() {
	return businessLicenseId;
    }

    public void setBusinessLicenseId(String businessLicenseId) {
	this.businessLicenseId = businessLicenseId;
    }

    public String getCompanyInfo() {
	return companyInfo;
    }

    public void setCompanyInfo(String companyInfo) {
	this.companyInfo = companyInfo;
    }

}
