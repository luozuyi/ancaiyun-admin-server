package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 店铺模板
 * 
 * @author Catch22
 * @date 2018年6月8日
 */
public class StoreTemplate implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 模板名字
     */
    private String templateName;
    /**
     * 模板路径
     */
    private String templateUrl;
    /**
     * 模板类型
     */
    private String templateType;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getTemplateName() {
	return templateName;
    }

    public void setTemplateName(String templateName) {
	this.templateName = templateName;
    }

    public String getTemplateUrl() {
	return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
	this.templateUrl = templateUrl;
    }

    public String getTemplateType() {
	return templateType;
    }

    public void setTemplateType(String templateType) {
	this.templateType = templateType;
    }

}
