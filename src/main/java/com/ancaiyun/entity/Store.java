package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * 店铺
 * 
 * @author Catch22
 * @date 2018年5月31日
 */
public class Store implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 商店名称
     */
    private String storeName;
    /**
     * 店主
     */
    private String storeOwer;
    /**
     * 店主名片
     */
    private String storeOwerCard;
    /**
     * 店铺电话
     */
    private String storeTelephone;
    /**
     * 店铺qq
     */
    private String storeQq;
    /**
     * 店铺网址
     */
    private String storeWw;
    /**
     * 外键 店铺级别id
     */
    private String storeGradeId;
    /**
     * 店铺地址
     */
    private String storeAreaId;
    /**
     * 是否推荐店铺
     */
    private Boolean storeRecommend;
    /**
     * 推荐时间
     */
    private Date storeRecommendTime;
    /**
     * 有效时间
     */
    private Date validityTime;
    /**
     * 执照附件 外键id
     */
    private String storeLicenseId;
    /**
     * 店铺信用
     */
    private Integer storeCredit;
    /**
     * seo关键字
     */
    private String storeSeoKeywords;
    /**
     * seo描述
     */
    private String storeSeoDescription;
    /**
     * 店铺信息
     */
    private String storeInfo;
    /**
     * 会员
     */
    private String memberId;
    /**
     * 店铺类型
     */
    private String storeCategoryId;
    /**
     * 子用户名
     */
    private String subUsername;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public String getDelFlag() {
	return delFlag;
    }

    public void setDelFlag(String delFlag) {
	this.delFlag = delFlag;
    }

    public String getStoreName() {
	return storeName;
    }

    public void setStoreName(String storeName) {
	this.storeName = storeName;
    }

    public String getStoreOwer() {
	return storeOwer;
    }

    public void setStoreOwer(String storeOwer) {
	this.storeOwer = storeOwer;
    }

    public String getStoreOwerCard() {
	return storeOwerCard;
    }

    public void setStoreOwerCard(String storeOwerCard) {
	this.storeOwerCard = storeOwerCard;
    }

    public String getStoreTelephone() {
	return storeTelephone;
    }

    public void setStoreTelephone(String storeTelephone) {
	this.storeTelephone = storeTelephone;
    }

    public String getStoreQq() {
	return storeQq;
    }

    public void setStoreQq(String storeQq) {
	this.storeQq = storeQq;
    }

    public String getStoreWw() {
	return storeWw;
    }

    public void setStoreWw(String storeWw) {
	this.storeWw = storeWw;
    }

    public String getStoreGradeId() {
	return storeGradeId;
    }

    public void setStoreGradeId(String storeGradeId) {
	this.storeGradeId = storeGradeId;
    }

    public String getStoreAreaId() {
	return storeAreaId;
    }

    public void setStoreAreaId(String storeAreaId) {
	this.storeAreaId = storeAreaId;
    }

    public Boolean getStoreRecommend() {
	return storeRecommend;
    }

    public void setStoreRecommend(Boolean storeRecommend) {
	this.storeRecommend = storeRecommend;
    }

    public Date getStoreRecommendTime() {
	return storeRecommendTime;
    }

    public void setStoreRecommendTime(Date storeRecommendTime) {
	this.storeRecommendTime = storeRecommendTime;
    }

    public Date getValidityTime() {
	return validityTime;
    }

    public void setValidityTime(Date validityTime) {
	this.validityTime = validityTime;
    }

    public String getStoreLicenseId() {
	return storeLicenseId;
    }

    public void setStoreLicenseId(String storeLicenseId) {
	this.storeLicenseId = storeLicenseId;
    }

    public Integer getStoreCredit() {
	return storeCredit;
    }

    public void setStoreCredit(Integer storeCredit) {
	this.storeCredit = storeCredit;
    }

    public String getStoreSeoKeywords() {
	return storeSeoKeywords;
    }

    public void setStoreSeoKeywords(String storeSeoKeywords) {
	this.storeSeoKeywords = storeSeoKeywords;
    }

    public String getStoreSeoDescription() {
	return storeSeoDescription;
    }

    public void setStoreSeoDescription(String storeSeoDescription) {
	this.storeSeoDescription = storeSeoDescription;
    }

    public String getStoreInfo() {
	return storeInfo;
    }

    public void setStoreInfo(String storeInfo) {
	this.storeInfo = storeInfo;
    }

    public String getMemberId() {
	return memberId;
    }

    public void setMemberId(String memberId) {
	this.memberId = memberId;
    }

    public String getStoreCategoryId() {
	return storeCategoryId;
    }

    public void setStoreCategoryId(String storeCategoryId) {
	this.storeCategoryId = storeCategoryId;
    }

    public String getSubUsername() {
	return subUsername;
    }

    public void setSubUsername(String subUsername) {
	this.subUsername = subUsername;
    }

    public void preInsert() {
	this.setId(UUID.randomUUID().toString().replaceAll("-", ""));
	this.setCreateTime(new Date());
	this.setDelFlag("0");
    }

}
