package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年5月31日
 */
@Mapper
@Repository
public interface ProductMapper extends BaseMapper{
    
//    /**
//     * 商品id查询商品详情
//     * @param id
//     * @return
//     */
//    Map<String, Object> selectByPrimaryKey(String id);
//    
//    /**
//     * 条件查询商品列表
//     * @param params
//     * @return
//     */
//    public List<Map<String, Object>> selectProductBySelection(Map<String, Object> params);
    
    /**
     * 修改商品信息
     * @param param
     * @return
     */
    int update(Map<String, Object> param);
    

}
