package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年5月31日
 */
@Mapper
@Repository
public interface StoreMapper extends BaseMapper{
    
    /**
     * 修改店铺状态(封禁/解除封禁店铺)
     * @param params
     */
    public int update(Map<String, Object> params);

}
