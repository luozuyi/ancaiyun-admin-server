package com.ancaiyun.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年6月8日
 */
@Mapper
@Repository
public interface QuickPurchaseMapper extends BaseMapper{

}
