package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年6月4日
 */
@Mapper
@Repository
public interface SelfSupportOrderMapper extends BaseMapper{
    
    /**
     * 修改订单状态信息
     * @param params
     */
    public int update(Map<String, Object> params);

}
