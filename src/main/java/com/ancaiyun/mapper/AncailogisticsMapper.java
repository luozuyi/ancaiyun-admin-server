package com.ancaiyun.mapper;

import com.ancaiyun.entity.Ancailogistics;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AncailogisticsMapper {

    /**
     * 查询所有物流信息
     * @return
     */
    List<Ancailogistics> getList(Map<String, String> params);

    /**
     * 查询所有物流发货信息
     * @param params
     * @return
     */
    List<Ancailogistics> getSendGoodsList(Map<String, String> params);

    /**
     * 根据id查询物流信息
     * @param id 主键id
     * @return
     */
    Ancailogistics getById(String id);

    /**
     * 修改物流信息
     * @param ancailogistics
     * @return
     */
    int update(Ancailogistics ancailogistics);
}
