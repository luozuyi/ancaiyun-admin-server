package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年6月4日
 */
@Mapper
@Repository
public interface SelfSupportStoreMapper extends BaseMapper{
    
    /**
     * 修改自营商城店铺信息
     * @param params
     */
    public int update(Map<String, Object> params);
}
