package com.ancaiyun.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.ancaiyun.entity.Accessory;
@Mapper
@Repository
public interface AccessoryMapper {
    
    int deleteByPrimaryKey(String id);

    int insert(Accessory record);

    int insertSelective(Accessory record);

    Accessory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Accessory record);

    int updateByPrimaryKey(Accessory record);
	
	public List<Map<String,Object>> selectAllProductPhotoBySelection(Map<String,Object> params);
	
	public List<Map<String,Object>> selectAllSelfProductPhotoBySelection(Map<String,Object> params);
}