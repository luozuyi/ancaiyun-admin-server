package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年5月31日
 */
@Mapper
@Repository
public interface SelfSupportProductMapper extends BaseMapper {
    
    /**
     * 修改优选商品信息
     * @param param
     * @return
     */
    int update(Map<String, Object> param);
    
   /**
    * 添加优选商品
    * @param param
    * @return
    */
    int insert(Map<String, Object> param);

}
