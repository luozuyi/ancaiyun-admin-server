package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年6月8日
 */
@Mapper
@Repository
public interface StoreTemplateMapper extends BaseMapper {
    
    /**
     * 删除店铺模板
     * @param params
     * @return
     */
    public int delete(Map<String, Object> params);
    
    /**
     * 添加店铺模板
     * @param params
     * @return
     */
    public int insert(Map<String, Object> params);

}
