package com.ancaiyun.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年6月8日
 */
@Mapper
@Repository
public interface InvoiceMapper extends BaseMapper{
    
    /**
     * 自营商城为买家开发票
     * @param param
     * @return
     */
    public int insert(Map<String, Object> param);
}
