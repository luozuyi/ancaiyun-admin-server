package com.ancaiyun.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Catch22
 * @date 2018年6月7日
 */
@Mapper
@Repository
public interface OrderproductInfoMapper extends BaseMapper{

}
