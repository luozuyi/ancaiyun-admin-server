package com.ancaiyun.bid.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;

/**
 * <p>Title: ContractAccessoryMapper</p>
 * <p>Description:合同附件Mapper </p>
 *
 * @author zhangzhanyang
 * @date 2018/6/11 11:35
 */
@Repository
@Mapper
public interface ContractAccessoryMapper {
    /*
    * @Description 查询合同附件列表
    * @Date 11:41 2018/6/11
    * @Param 
    * @return 
    **/
    
    List<ModelMap> getByContractId(String id);
}
