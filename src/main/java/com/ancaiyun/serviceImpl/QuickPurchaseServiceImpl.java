package com.ancaiyun.serviceImpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ancaiyun.mapper.QuickPurchaseMapper;
import com.ancaiyun.service.QuickPurchaseService;

/**
 * @author Catch22
 * @date 2018年6月8日
 */
@Transactional
@Service
public class QuickPurchaseServiceImpl extends BaseServiceImpl<QuickPurchaseMapper> implements QuickPurchaseService {


}
