package com.ancaiyun.serviceImpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ancaiyun.mapper.SelfSupportOrderInfoMapper;
import com.ancaiyun.service.SelfSupportOrderInfoService;

/**
 * 自营订单商品详情接口实现类
 * @author Catch22
 * @date 2018年6月5日
 */
@Transactional
@Service
public class SelfSupportOrderInfoServiceImpl extends BaseServiceImpl<SelfSupportOrderInfoMapper> implements SelfSupportOrderInfoService {

}
