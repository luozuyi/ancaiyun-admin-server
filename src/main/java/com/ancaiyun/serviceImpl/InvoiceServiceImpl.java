package com.ancaiyun.serviceImpl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ancaiyun.mapper.InvoiceMapper;
import com.ancaiyun.service.InvoiceService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.Result;

/**
 * 发票管理接口实现类
 * @author Catch22
 * @date 2018年6月8日
 */
@Transactional
@Service
public class InvoiceServiceImpl extends BaseServiceImpl<InvoiceMapper> implements InvoiceService {
    
    @Autowired
    private InvoiceMapper invoiceMapper;

    /* (non-Javadoc)
     * @see com.ancaiyun.service.InvoiceService#insertInvoice(java.util.Map)
     */
	@Override
	public Result insertInvoice(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			params.put("id", UUID.randomUUID().toString().replaceAll("-", ""));
			params.put("delFlag", "0");
			params.put("createTime", new Date());
			int col = invoiceMapper.insert(params);
			if (col == 1) {
				code = Constants.SUCCESS;
				msg = "成功";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
