package com.ancaiyun.serviceImpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ancaiyun.mapper.StoreCollectMapper;
import com.ancaiyun.service.StoreCollectService;

/**
 * 店铺收藏接口实现类
 * @author Catch22
 * @date 2018年6月4日
 */
@Transactional
@Service
public class StoreCollectServiceImpl extends BaseServiceImpl<StoreCollectMapper> implements StoreCollectService {


}
