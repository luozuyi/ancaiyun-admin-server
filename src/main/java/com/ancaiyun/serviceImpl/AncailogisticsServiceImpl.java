package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.Ancailogistics;
import com.ancaiyun.mapper.AncailogisticsMapper;
import com.ancaiyun.service.AncailogisticsService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.PageHelperNew;
import com.ancaiyun.utils.Result;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AncailogisticsServiceImpl implements AncailogisticsService {

    @Autowired
    private AncailogisticsMapper ancailogisticsMapper;

    @Override
    public Result listPage(Integer pageNum, Integer pageSize, Map<String, String> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "失败";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Ancailogistics> ancailogisticsList = ancailogisticsMapper.getList(params);
            PageInfo<Ancailogistics> page = new PageInfo<>(ancailogisticsList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e){
            code = Constants.ERROR;
            msg = "后台出错";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result sendGoodsListPage(Integer pageNum, Integer pageSize, Map<String, String> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "失败";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Ancailogistics> ancailogisticsList = ancailogisticsMapper.getSendGoodsList(params);
            PageInfo<Ancailogistics> page = new PageInfo<>(ancailogisticsList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e){
            code = Constants.ERROR;
            msg = "后台出错";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateStatus(String id, String transactionStatus, String logisticsStatus) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "失败";
        try {
            Ancailogistics ancailogistics = ancailogisticsMapper.getById(id);
            if (ancailogistics != null){
                if (transactionStatus != null && logisticsStatus == null && ancailogistics.getTransactionStatus().equals("0")){
                    ancailogistics.setTransactionStatus(transactionStatus);
                    if (transactionStatus.equals("1")){
                        ancailogistics.setLogisticsStatus("0");
                    }
                } else if (transactionStatus == null && logisticsStatus != null && ancailogistics.getTransactionStatus().equals("1")){
                    ancailogistics.setLogisticsStatus(logisticsStatus);
                }
                ancailogisticsMapper.update(ancailogistics);
                code = Constants.SUCCESS;
                msg = "成功";
            } else {
                msg = "该记录不存在";
            }
        } catch (Exception e){
            code = Constants.ERROR;
            msg = "后台出错";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
