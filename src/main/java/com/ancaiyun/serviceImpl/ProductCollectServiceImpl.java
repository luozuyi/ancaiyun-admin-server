package com.ancaiyun.serviceImpl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ancaiyun.mapper.ProductCollectMapper;
import com.ancaiyun.service.ProductCollectService;

/**
 * 商品收藏实现类
 * @author Catch22
 * @date 2018年6月4日
 */
@Transactional
@Service
public class ProductCollectServiceImpl extends BaseServiceImpl<ProductCollectMapper> implements ProductCollectService {
    
}
