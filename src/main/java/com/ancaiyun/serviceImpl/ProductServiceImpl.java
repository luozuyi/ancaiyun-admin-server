package com.ancaiyun.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ancaiyun.mapper.AccessoryMapper;
import com.ancaiyun.mapper.ProductMapper;
import com.ancaiyun.mapper.SelfSupportStoreMapper;
import com.ancaiyun.service.ProductService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.Result;

/**
 * 商品实现类
 * @author Catch22
 * @date 2018年6月2日
 */
@Transactional
@Service
public class ProductServiceImpl extends BaseServiceImpl<SelfSupportStoreMapper>  implements ProductService {

    @Autowired
    private ProductMapper productMapper;
    
    @Autowired
    private AccessoryMapper accessoryMapper;
    
	@Override
	public Result updateProduct(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> product = productMapper.selectByPrimaryKey(params.get("id").toString());
			if (product != null) {
				int col = productMapper.update(params);
				if (col == 1) {
					code = Constants.SUCCESS;
					msg = "成功";
				}
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}
	
	@Override
	public Result selectProductByPrimaryKey(String id) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> maps = productMapper.selectByPrimaryKey(id);
			if (maps != null) {
				Map<String, Object> photo = new HashMap<String, Object>();
				String productId = maps.get("id").toString();
				photo.put("productId", productId);
				photo.put("delFlag", "0");
				List<Map<String, Object>> photoMap = accessoryMapper.selectAllProductPhotoBySelection(photo);
				maps.putIfAbsent("photo", photoMap);
				
				result.setData(maps);
				code = Constants.SUCCESS;
				msg = "成功";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
