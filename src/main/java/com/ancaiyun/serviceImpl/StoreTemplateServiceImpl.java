package com.ancaiyun.serviceImpl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ancaiyun.mapper.StoreTemplateMapper;
import com.ancaiyun.service.StoreTemplateService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.Result;

/**
 * 店铺模板管理接口实现类
 * 
 * @author Catch22
 * @date 2018年6月8日
 */
@Transactional
@Service
public class StoreTemplateServiceImpl extends BaseServiceImpl<StoreTemplateMapper> implements StoreTemplateService {

	@Autowired
	private StoreTemplateMapper templateMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ancaiyun.service.StoreTemplateService#insertTemplate(java.util.Map)
	 */
	@Override
	public Result insertTemplate(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			params.put("id", UUID.randomUUID().toString().replaceAll("-", ""));
			params.put("delFlag", "0");
			params.put("createTime", new Date());
			int col = templateMapper.insert(params);
			if (col == 1) {
				code = Constants.SUCCESS;
				msg = "成功";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ancaiyun.service.StoreTemplateService#delteTemplate(java.util.Map)
	 */
	@Override
	public Result delteTemplate(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> product = templateMapper.selectByPrimaryKey(params.get("id").toString());
			if (product != null) {
				int col = templateMapper.delete(params);
				if (col == 1) {
					code = Constants.SUCCESS;
					msg = "成功";
				}
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
