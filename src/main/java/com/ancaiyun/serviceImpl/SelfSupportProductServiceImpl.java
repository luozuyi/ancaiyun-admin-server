package com.ancaiyun.serviceImpl;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ancaiyun.mapper.SelfSupportProductMapper;
import com.ancaiyun.service.SelfSupportProductService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.RandomCodeUtil;
import com.ancaiyun.utils.Result;

/**
 * 自营商城商品接口实现类
 * 
 * @author Catch22
 * @date 2018年6月4日
 */
@Transactional
@Service
public class SelfSupportProductServiceImpl extends BaseServiceImpl<SelfSupportProductMapper>
		implements SelfSupportProductService {

	@Autowired
	private SelfSupportProductMapper productMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ancaiyun.service.SelfSupportProductService#insertProduct(java.util.
	 * Map)
	 */
	@Override
	public Result insertProduct(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			params.put("id", UUID.randomUUID().toString().replaceAll("-", ""));
			params.put("delFlag", "0");
			params.put("createTime", new Date());
			params.put("productSerial", "ZYSP" + RandomCodeUtil.getRandomCode());
			int col = productMapper.insert(params);
			if (col == 1) {
				code = Constants.SUCCESS;
				msg = "成功";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ancaiyun.service.SelfSupportProductService#updateProduct(java.util.
	 * Map)
	 */
	@Override
	public Result updateProduct(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> product = productMapper.selectByPrimaryKey(params.get("id").toString());
			if (product != null) {
				int col = productMapper.update(params);
				if (col == 1) {
					code = Constants.SUCCESS;
					msg = "成功";
				}
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
