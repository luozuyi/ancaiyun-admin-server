package com.ancaiyun.serviceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ancaiyun.mapper.AccessoryMapper;
import com.ancaiyun.mapper.SelfSupportOrderInfoMapper;
import com.ancaiyun.mapper.SelfSupportOrderMapper;
import com.ancaiyun.service.SelfSupportOrderService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.PageHelperNew;
import com.ancaiyun.utils.Result;
import com.github.pagehelper.PageInfo;

/**
 * 自营商城订单接口实现类
 * @author Catch22
 * @date 2018年6月5日
 */
@Transactional
@Service
public class SelfSupportOrderServiceImpl implements SelfSupportOrderService {

	@Autowired
	private SelfSupportOrderMapper orderMapper;
	
	@Autowired
	private SelfSupportOrderInfoMapper infoMapper;
	
	@Autowired
	private AccessoryMapper accessoryMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ancaiyun.service.SelfSupportOrderService#update(java.util.Map)
	 */
	@Override
	public Result update(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> product = orderMapper.selectByPrimaryKey(params.get("id").toString());
			if (product != null) {
				int col = orderMapper.update(params);
				if (col == 1) {
					code = Constants.SUCCESS;
					msg = "成功";
				}
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}
	
	@Override
	public Result listPage(Integer pageNum, Integer pageSize, Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			params.put("delFlag", "0");
			PageHelperNew.startPage(pageNum, pageSize);
			List<Map<String, Object>> maps = orderMapper.selectAllBySelection(params);
			for (Map<String, Object> map : maps) {
				String orderId = map.get("id").toString();
				Map<String, Object> info = new HashMap<String, Object>();
				info.put("selfSupportOrderId", orderId);
				info.put("delFlag", "0");
				List<Map<String, Object>> infoMap = infoMapper.selectAllBySelection(info);
				for (Map<String, Object> map2 : infoMap) {
					String productId = map2.get("selfSupportProductId").toString();
					Map<String, Object> photo = new HashMap<String, Object>();
					photo.put("selfSupportProductId", productId);
					photo.put("delFlag", "0");
					List<Map<String, Object>> photoMap = accessoryMapper.selectAllSelfProductPhotoBySelection(photo);
					map2.putIfAbsent("photo", photoMap);
				}
				map.put("info", infoMap);
			}
			PageInfo<Map<String, Object>> page = new PageInfo<>(maps);
			code = Constants.SUCCESS;
			msg = "成功";
			result.setData(page);
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}
    
    
	@Override
	public Result selectByPrimaryKey(String id) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> maps = orderMapper.selectByPrimaryKey(id);
			if (maps != null) {
				String orderId = maps.get("id").toString();
				Map<String, Object> info = new HashMap<String, Object>();
				info.put("selfSupportOrderId", orderId);
				info.put("delFlag", "0");
				List<Map<String, Object>> infoMap = infoMapper.selectAllBySelection(info);
				for (Map<String, Object> map2 : infoMap) {
					String productId = map2.get("selfSupportProductId").toString();
					Map<String, Object> photo = new HashMap<String, Object>();
					photo.put("selfSupportProductId", productId);
					photo.put("delFlag", "0");
					List<Map<String, Object>> photoMap = accessoryMapper.selectAllSelfProductPhotoBySelection(photo);
					map2.putIfAbsent("photo", photoMap);
				}
				maps.put("info", infoMap);
				result.setData(maps);
				code = Constants.SUCCESS;
				msg = "成功";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
