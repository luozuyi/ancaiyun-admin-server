package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.Accessory;
import com.ancaiyun.mapper.AccessoryMapper;
import com.ancaiyun.service.AccessoryService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.Result;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.UUID;

@Transactional
@Service
public class AccessoryServiceImpl implements AccessoryService{
    @Autowired
    private FastFileStorageClient fastFileStorageClient;
    @Autowired
    private AccessoryMapper accessoryMapper;
    @Override
    public Result addArticleCover(MultipartFile file) {
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
            String fileUrl = storePath.getFullPath();
            Accessory accessory = new Accessory();
            accessory.setId(UUID.randomUUID().toString().replace("-", ""));
            accessory.setCreateTime(new Date());
            accessory.setFilePath(storePath.getPath());
            accessory.setGroupName(storePath.getGroup());
            accessory.setFileRealName(file.getOriginalFilename());
            accessory.setExt(fileUrl.substring(fileUrl.lastIndexOf(".") + 1));
            accessoryMapper.insertSelective(accessory);
            result.setData(accessory);
            msg = "成功";
            code = "0";
        } catch (Exception e) {
            msg = "上传出错";
            code = "-2";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
