package com.ancaiyun.serviceImpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ancaiyun.mapper.StoreMapper;
import com.ancaiyun.service.StoreService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.Result;

/**
 * 店铺接口实现类
 * 
 * @author Catch22
 * @date 2018年6月1日
 */
@Transactional
@Service
public class StoreServiceImpl extends BaseServiceImpl<StoreMapper> implements StoreService {

	@Autowired
	private StoreMapper storeMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ancaiyun.service.StoreService#updateStore(java.util.Map)
	 */
	@Override
	public Result updateStore(Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> store = storeMapper.selectByPrimaryKey(params.get("id").toString());
			if (store != null) {
				int col = storeMapper.update(params);
				if (col == 1) {
					code = Constants.SUCCESS;
					msg = "成功";
				}
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
