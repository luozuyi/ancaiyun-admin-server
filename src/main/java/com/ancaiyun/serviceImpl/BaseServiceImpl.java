package com.ancaiyun.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.ancaiyun.mapper.BaseMapper;
import com.ancaiyun.service.BaseService;
import com.ancaiyun.utils.Constants;
import com.ancaiyun.utils.PageHelperNew;
import com.ancaiyun.utils.Result;
import com.github.pagehelper.PageInfo;

/**
 * 公共查询接口实现类
 * @author Catch22
 * @date 2018年6月6日
 */
public abstract class BaseServiceImpl<M extends BaseMapper> implements BaseService<Integer,Integer,Map<String,Object>, String> {
    
    @Autowired
    private M mapper;
    
    /* (non-Javadoc)
     * @see com.ancaiyun.service.BaseService#listPage(java.lang.Object, java.lang.Object, java.lang.Object)
     */
	@Override
	public Result listPage(Integer pageNum, Integer pageSize, Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			if (!params.containsKey("delFlag")) {
				params.put("delFlag", "0");
			}
			PageHelperNew.startPage(pageNum, pageSize);
			List<Map<String, Object>> maps = mapper.selectAllBySelection(params);
			PageInfo<Map<String, Object>> page = new PageInfo<>(maps);
			code = Constants.SUCCESS;
			msg = "成功";
			result.setData(page);
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

    /* (non-Javadoc)
     * @see com.ancaiyun.service.BaseService#selectByPrimaryKey(java.lang.Object)
     */
	@Override
	public Result selectByPrimaryKey(String id) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			Map<String, Object> maps = mapper.selectByPrimaryKey(id);
			if (maps != null) {
				result.setData(maps);
				code = Constants.SUCCESS;
				msg = "成功";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
