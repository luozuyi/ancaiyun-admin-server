package com.ancaiyun.service;

import java.util.Map;

/**
 * 优选商品收藏接口
 * @author Catch22
 * @date 2018年6月4日
 */
public interface ProductCollectService extends BaseService<Integer, Integer, Map<String,Object>, String> {
    
}
