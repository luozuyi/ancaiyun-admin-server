package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 自营商城商品接口
 * @author Catch22
 * @date 2018年6月2日
 */
public interface SelfSupportProductService extends BaseService<Integer, Integer, Map<String,Object>, String>{
    
    /**
     * 添加自营商品
     * @param params
     * @return
     */
    Result insertProduct(Map<String,Object> params);

    /**
     * 修改自营商品
     * @param params
     * @return
     */
    Result updateProduct(Map<String, Object> params);

}
