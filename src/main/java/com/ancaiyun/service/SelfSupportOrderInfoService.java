package com.ancaiyun.service;

import java.util.Map;

/**
 * 自营订单商品详情接口
 * @author Catch22
 * @date 2018年6月5日
 */
public interface SelfSupportOrderInfoService extends BaseService<Integer, Integer, Map<String,Object>, String> {

}
