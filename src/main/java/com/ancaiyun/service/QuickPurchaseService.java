package com.ancaiyun.service;

import java.util.Map;

/**
 * 极速采购接口
 * @author Catch22
 * @date 2018年6月4日
 */
public interface QuickPurchaseService extends BaseService<Integer, Integer, Map<String,Object>, String> {
    
}
