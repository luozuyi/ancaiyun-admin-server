package com.ancaiyun.service;

import com.ancaiyun.utils.Result;
import org.springframework.web.multipart.MultipartFile;

public interface AccessoryService {
    /**
     * 添加文章封面
     * @param file
     * @return
     */
    Result addArticleCover(MultipartFile file);
}
