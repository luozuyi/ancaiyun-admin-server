package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 自营订单接口
 * @author Catch22
 * @date 2018年6月5日
 */
public interface SelfSupportOrderService {
    
    /**
     * 修改自营订单状态
     * @param params
     * @return
     */
    Result update(Map<String,Object> params);
    
    /**
     * 条件查询订单列表
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    public Result listPage(Integer pageNum, Integer pageSize,Map<String,Object> params);
    
    /**
     * id查询订单详情
     * @param id
     * @return
     */
	Result selectByPrimaryKey(String id);
    

}
