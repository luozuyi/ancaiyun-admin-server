package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 店铺接口
 * @author Catch22
 * @date 2018年6月1日
 */
public interface StoreService extends BaseService<Integer, Integer, Map<String,Object>, String>{
    
    /**
     * 修改店铺状态(封禁/解除封禁店铺)
     * @param params
     * @return
     */
    Result updateStore(Map<String, Object> params);

}
