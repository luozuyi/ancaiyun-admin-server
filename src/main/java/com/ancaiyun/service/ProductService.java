package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 商品接口
 * @author Catch22
 * @date 2018年6月2日
 */
public interface ProductService extends BaseService<Integer, Integer, Map<String,Object>, String> {
    
    /**
     * 修改商品状态(上下架 违规处理)
     * @param params
     * @return
     */
    Result updateProduct(Map<String, Object> params);
	
	/**
	 * id查询商品详情
	 * @param id
	 * @return
	 */
	Result selectProductByPrimaryKey(String id);

}
