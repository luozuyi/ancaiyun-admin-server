package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 自营店铺接口
 * @author Catch22
 * @date 2018年6月1日
 */
public interface SelfSupportStoreService  {
    
    /**
     * 修改自营商城店铺信息
     * @param params
     * @return
     */
    Result updateStore(Map<String, Object> params);
    
    /**
     * 查询自营店铺详情
     * @param id
     * @return
     */
    Result selectByPrimaryKey(String id);

}
