package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 发票管理接口
 * @author Catch22
 * @date 2018年6月8日
 */
public interface InvoiceService extends BaseService<Integer, Integer, Map<String,Object>, String> {
    
    /**
     * 自营商城为买家开发票
     * @param params
     * @return
     */
    Result insertInvoice(Map<String,Object> params);
}
