package com.ancaiyun.service;

import com.ancaiyun.utils.Result;

/**
 * 公共查询接口
 * @author Catch22
 * @date 2018年6月6日
 */
public interface BaseService<A,B,T,ID> {

    /**
     * 条件查询
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    Result listPage(A pageNum, B pageSize, T params);

    /**
     * id查询
     * @param id
     * @return
     */
    Result selectByPrimaryKey(ID id);

}
