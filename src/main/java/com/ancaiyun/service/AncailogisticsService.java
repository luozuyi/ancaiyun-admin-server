package com.ancaiyun.service;

import com.ancaiyun.utils.Result;

import java.util.Map;

public interface AncailogisticsService {

    /**
     * 一页显示多少条
     * @param pageNum 当前页数
     * @param pageSize 一页显示多少条
     * @param params 查询参数
     * @return
     */
    Result listPage(Integer pageNum, Integer pageSize, Map<String, String> params);

    /**
     * 分页查询安采物流发货列表
     * @param pageNum 当前页数
     * @param pageSize 一页显示多少条
     * @param params 查询参数
     * @return
     */
    Result sendGoodsListPage(Integer pageNum, Integer pageSize, Map<String,String> params);

    /**
     * 修改物流状态
     * @param id 主键id
     * @param transactionStatus 交易状态 0:待联系 1:达成交易 2:未达成交易
     * @param logisticsStatus 物流状态 0:待发货 1:已发货 2:已收货
     * @return
     */
    Result updateStatus(String id, String transactionStatus, String logisticsStatus);
}
