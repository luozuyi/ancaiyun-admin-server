package com.ancaiyun.service;

import java.util.Map;

import com.ancaiyun.utils.Result;

/**
 * 店铺模板管理接口
 * @author Catch22
 * @date 2018年6月2日
 */
public interface StoreTemplateService extends BaseService<Integer, Integer, Map<String,Object>, String>{
    
    /**
     * 添加店铺模板
     * @param params
     * @return
     */
    Result insertTemplate(Map<String,Object> params);

    /**
     * 删除店铺模板
     * @param params
     * @return
     */
    Result delteTemplate(Map<String, Object> params);

}
