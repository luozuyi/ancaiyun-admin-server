package com.ancaiyun.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Catch22
 * @date 2018年6月11日
 */
public abstract class RandomCodeUtil {

    public static String getRandomCode() {
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	String code = sdf.format(date);
	int a = (int) ((Math.random() * 9 + 1) * 1000);
	return code + a;
    }
}
